import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { IonCard, IonImg, IonCardHeader, IonCardSubtitle, IonCardTitle, IonItem, IonInput, IonGrid, IonRow, IonCol } from '@ionic/react';
import { useState } from 'react';
import './Pokemons.css';
import { Pokemon } from '../../interface/Interface';
import axios from 'axios';

import NullPokemon from "./images/nullpokemon.jpg";

const svgSearch = 
<svg width="24" height="24" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M29.1804 31.9902L32.0088 29.1618L44.0298 41.1824L41.2014 44.0108L29.1804 31.9902Z" fill="#616161"/>
  <path d="M20 4C15.7565 4 11.6869 5.68571 8.68629 8.68629C5.68571 11.6869 4 15.7565 4 20C4 24.2435 5.68571 28.3131 8.68629 31.3137C11.6869 34.3143 15.7565 36 20 36C24.2435 36 28.3131 34.3143 31.3137 31.3137C34.3143 28.3131 36 24.2435 36 20C36 15.7565 34.3143 11.6869 31.3137 8.68629C28.3131 5.68571 24.2435 4 20 4V4Z" fill="#616161"/>
  <path d="M32.4558 35.3409L35.2842 32.5125L43.9818 41.2097L41.1534 44.0382L32.4558 35.3409Z" fill="#37474F"/>
  <path d="M20 7C16.5522 7 13.2456 8.36964 10.8076 10.8076C8.36964 13.2456 7 16.5522 7 20C7 23.4478 8.36964 26.7544 10.8076 29.1924C13.2456 31.6304 16.5522 33 20 33C23.4478 33 26.7544 31.6304 29.1924 29.1924C31.6304 26.7544 33 23.4478 33 20C33 16.5522 31.6304 13.2456 29.1924 10.8076C26.7544 8.36964 23.4478 7 20 7V7Z" fill="#64B5F6"/>
  <path d="M26.9001 14.2C25.2001 12.2 22.7001 11 20.0001 11C17.3001 11 14.8001 12.2 13.1001 14.2C12.7001 14.6 12.8001 15.3 13.2001 15.6C13.6001 16 14.3001 15.9 14.6001 15.5C16.0001 13.9 17.9001 13 20.0001 13C22.1001 13 24.0001 13.9 25.4001 15.5C25.6001 15.7 25.9001 15.9 26.2001 15.9C26.4001 15.9 26.7001 15.8 26.8001 15.7C27.2001 15.3 27.2001 14.6 26.9001 14.2Z" fill="#BBDEFB"/>
</svg>


const Pokemons: React.FC = () => {

  const [pokemons, setPokemons] = useState<Pokemon>();
  const [title, setTitle] = useState('')

  const getPosts = async () => {

    if(title) {
      try {
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${title}`);
        setPokemons(res.data as Pokemon);
      } catch (err) {
        const errorData: any = {
          id: 0,
          name: 'El Pokemon no existe',
          sprites: {
            front_default: null
          }
        }
        setPokemons(errorData as Pokemon);
      }
    }
  }

  return (
    <IonPage>
      <IonHeader>
      </IonHeader>
      <IonContent>

          <div id='pokemons' className='container'>

              <IonGrid id='grid-pokemons' class='pokedex-wallpaper'>

                <IonRow >

                  <IonCol className='container col-pokemons'>
                    <h4 className='text-center' style={{marginTop: '22px', fontSize: '3.3rem' ,fontWeight: 'bold'}}>
                      ¿Que pokemon estas buscando?
                    </h4>
                  </IonCol>

                  <IonCol className='border-grid'>    

                    <div className='container'>

                      <div className="alert alert-light" style={{marginTop: '22px', fontWeight: 'bold'}} role="alert">
                        <p> 
                          Ingresar Nombre de Pokemon o Id
                        </p>

                        <IonItem>
                            <IonInput value={title} placeholder="Ingresar" onIonChange={e => setTitle(e.detail.value!)}></IonInput>
                            <button type="button" onClick={getPosts} className="btn btn-outline-warning" style={{fontWeight: 'bold'}}  >
                              Buscar&nbsp;&nbsp; {svgSearch}
                            </button>
                        </IonItem>
                      </div>
                      
                    </div>

                    {!pokemons ? null :
                      <IonCard key={pokemons.id}>
                        <IonCardHeader>
                          { 
                            pokemons.sprites.front_default ? <IonImg className="size" src={pokemons.sprites.front_default}></IonImg> : <IonImg className="size" src={NullPokemon}></IonImg> 
                          }
                          
                          {
                            pokemons.id === 0 ? <IonCardSubtitle style={{textAlign: 'center', fontWeight: 'bold'}}>Error</IonCardSubtitle> : <IonCardSubtitle style={{textAlign: 'center'}}>Id: {pokemons.id}</IonCardSubtitle>
                          }
                          
                          <IonCardTitle style={{textAlign: 'center', fontWeight: 'bold'}}>{pokemons.name}</IonCardTitle>
                        </IonCardHeader>
                      </IonCard>   
                    }

                  </IonCol>

                </IonRow>

              </IonGrid>
          </div>

      </IonContent>
    </IonPage>
  );
};

export default Pokemons;

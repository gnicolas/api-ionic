import { IonContent, IonHeader, IonPage } from '@ionic/react';
import './Home.css';

import Tierra from "./images/tierra.png";
import Fuergo from "./images/fuego.png";
import Agua from "./images/agua.png";
import Electrico from "./images/electrico.png";
import Psiquico from "./images/psiquico.png";
import Normal from "./images/normal.png";

import CardResponsive from '../../components/CardResponsive';

const title: string = "Pokemon Api";

function createData(title: string, img: string, description: string, color: string) {
  return { title, img, description, color };
}

const typesPokemons = [
  createData('Tierra', Tierra, 'Elemento', '#D5F5E3'),
  createData('Fuego', Fuergo, 'Elemento', '#F2D7D5'),
  createData('Agua', Agua, 'Elemento', '#D4E6F1'),
  createData('Eléctrico', Electrico, 'Elemento', '#FCF3CF'),
  createData('Psíquico', Psiquico, 'Elemento', '#E8DAEF'),
  createData('Normal', Normal, 'Elemento', '#F6DDCC')
];

const Home: React.FC = () => {


  return (
    <IonPage >
      <IonHeader>
      </IonHeader>
      <IonContent>

        <div className='container' style={{marginBottom: '111px'}}>

          <h1 style={{marginTop: '69px'}}>{title}</h1>

          <div className="row" >
            <div className="col-sm-6" style={{marginTop: '69px'}}>
              <a href='/pokemons' style={{textDecoration: 'none'}}>
                <div className="card card-menu">
                  <div className="card-body pokeball">
                    <p className="card-text text-pokeball">BUSQUEDA</p>
                    <h1 className="card-title text-pokeball" >¡Atrapa tu pokemon!</h1>
                  </div>
                </div>  
              </a>
              
            </div>
            <div className="col-sm-6" style={{marginTop: '69px'}}>
              <a href='/tienda' style={{textDecoration: 'none'}}>
                <div className="card card-menu">
                  <div className="card-body pokemon">
                    <h1 className="card-title text-pokemon-shop">Equipate!</h1>
                  </div>
                </div>  
              </a>
            </div>
          </div>


          <h1 style={{marginTop: '69px'}}>Tipos de pokemons</h1>
          <CardResponsive cardHeight='111px' groups={typesPokemons} colResponsive='col-sm-4' rowMargin='11px' buttonsResponse='' />

        </div>

      </IonContent>
    </IonPage>
  );
};

export default Home;
